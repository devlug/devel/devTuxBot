#==========================================================
# eggdrop - devTuxBot
#==========================================================

#---------------------------------------
# Konfiguration für devTuxBot
#---------------------------------------

# Bot Version
set botversion "0.0.19"
# Not Name
set botname "devTuxBot"
# absolute path to ansiweather
set ansiweather "ansiweather"
# mod file
set mod_file "/home/devlug/devtuxbot/mod.txt"
# help message
set help_file "/home/devlug/devtuxbot/help.txt"
# source our credentials (irc_password, git_token)
source /home/devlug/devtuxbot/credentials.tcl

#==========================================================
# Bindings
#==========================================================

#------------------------------------------------------------------------------
# Befehle die von jedem ausgeführt werden können
#------------------------------------------------------------------------------
bind pub - !tux pub_tux
bind pub - !help pub_tux
bind pub - !iam pub_iam
bind pub - !staff pub_staff
bind pub - !s say_stammtisch
bind pub - !r pub_remind

#------------------------------------------------------------------------------
# Befehle die von Personen mit +v (voice) aufgerufen werden können.
#------------------------------------------------------------------------------
bind pub v !w pub_weather
bind pub v !cal pub_cal
bind pub v !member pub_member
bind pub v !topadd pub_top_add
bind pub v !toplist pub_top_list
bind pub v !issue pub_issue
bind pub v !cve pub_cve
bind pub v !webcheck pub_webcheck
bind pub v !mod pub_mod

#------------------------------------------------------------------------------
# Befehle die von Personen mit +m (master) aufgerufen werden können.
#------------------------------------------------------------------------------
bind pub m !go pub_go
bind pub m !task pub_task
bind pub m !task_done pub_task_done
bind pub m !topic pub_topic

#------------------------------------------------------------------------------
# Befehle die von Personen mit +n (owner) aufgerufen werden können.
#------------------------------------------------------------------------------
bind pub n !update pub_update

#------------------------------------------------------------------------------
# Auto join Template
#------------------------------------------------------------------------------
#bind join - * say_hello
#proc say_hello {nick uhost handle chan} {
#	putmsg $nick "Hallo $nick."
#}

#------------------------------------------------------------------------------
# Timer
#------------------------------------------------------------------------------

bind time - "00 06 * * *" say_goodmorning	;# Timer um 06:00
# be quiet
#bind time - "00 18 * * *" say_goodevening	;# Timer um 18:00
#bind time - "30 20 * * *" say_stammtisch	;# Timer um 20:30

#bind join - * channeljoin:wjoin
bind evnt - init-server evnt:init_server

#---------------------------------------
# Nickserv identify
#---------------------------------------
proc evnt:init_server {type} {
	global botversion
	global botname
	global irc_password

	putserv "privmsg nickserv :identify $irc_password" -next
	putlog "- $botname $botversion registering..."
}

#------------------------------------------------------------------------------
# Zieht die aktuelle Version von gitlab via Shellscript (updateBot)
# und macht dann ein rehash.
#------------------------------------------------------------------------------
proc pub_update {nick uhost handle chan arg} {
	putmsg $chan "Ups,... alles klar! Drückt mir die Daumen! Starte devTuxBot update..."
	set retval [catch { exec updateBot } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
		putmsg $nick "$rec"
	}
	putmsg $nick "Rehash..."
	rehash
	putmsg $chan "Done!"
}

#------------------------------------------------------------------------------
# Timed greetings
#------------------------------------------------------------------------------
proc say_stammtisch {a c d e f} {
	set retval [catch { exec stammtisch --irc } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
		putmsg #devlug "$rec"
	}
}

proc say_goodmorning {a c d e f} {
#	set current [ctime [unixtime]]
#	putmsg #devlug "Guten Morgen, devLUG! Es ist $current! Wir wünschen euch einen schönen Tag."

	set command remind
	lappend command -h
	lappend command /home/devlug/.reminders
	set retval [ catch { exec {*}$command } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
		putmsg #devlug "$rec"
	}

}

proc say_goodevening {a c d e f} {
	set current [ctime [unixtime]]
	putmsg #devlug "Guten Abend, devLUG! Es ist $current!"
}

#------------------------------------------------------------------------------
# Staff suchen
#------------------------------------------------------------------------------
proc pub_staff {nick uhost handle chan arg} {
	putmsg $chan "Hallo $nick. Du brauchst Hilfe? Ich guck mal ob ich jemand finde,..."
	set stafflist [userlist o]
	putmsg $chan "Hi $stafflist. Kann jemand $nick weiterhelfen? $nick, einfach mal abwarten. Vielleicht kommt irgendwann jemand vom Personal."
}

#------------------------------------------------------------------------------
# devlug starten
#------------------------------------------------------------------------------
proc pub_go {nick uhost handle chan arg} {
	putmsg $chan " "
	putmsg $chan " "
	putmsg $chan " ===== Willkommen zur #devlug ====="
	putmsg $chan " "
	putmsg $chan " "
	putmsg $chan "Hallo /dev/LUG! Willkommen zu unserem Stammtisch. Wir beginnen wie immer mit dem offiziellen Teil. "
	set retval [catch { exec task +top } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
		putmsg $chan "$rec"
	}
	putmsg $chan "Bitte mal kurz Handzeichen o/ die gerade anwesend sind."
	
	# neues IRC-Topic setzten
	pub_topic 1 2 3 $chan 5
	
	# neuen Termin auf HP 
	set command set-hp-date.sh
	set retval [ catch { exec {*}$command } msg ]
}

#------------------------------------------------------------------------------
# zeige alle devlug Member
#------------------------------------------------------------------------------
proc pub_member {nick uhost handle chan arg} {
	putmsg $chan "Hallo"
	set nicklist [userlist]
	putmsg $chan $nicklist
}

#------------------------------------------------------------------------------
# alle Infos zu User anzeigen
#------------------------------------------------------------------------------
proc pub_iam {nick uhost handle chan arg} {
	putmsg $chan "Hallo $nick. Deine Host ist $uhost. handle $handle chan $chan arg $arg"
	set infoline [getuser $handle INFO]
	putmsg $chan "Deine Infoline: $infoline"
	set notesc [notes $nick]
	putmsg $chan "Ich habe $notesc Nachichten für dich"
}

#------------------------------------------------------------------------------
# Kalender
# obsolete weil doppelt? (siehe: task calendar)
#------------------------------------------------------------------------------
proc pub_cal {nick uhist hand chan arg} {
	set retval [catch { exec cal } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
		putmsg $chan "$rec"
	}
}

proc pub_remind {nick uhist hand chan arg} {
	set option "/home/devlug/.reminders"
	set retval [catch { exec remind $option } msg ]
	set records [split $msg "\n"]
	foreach rec $records {
    	    putmsg $chan "$rec"
	}
}

#------------------------------------------------------------------------------
# alternativer 1zu1 wrapper für task
# todo: forbidden_args um weitere kommandos erweitern
#------------------------------------------------------------------------------
proc pub_task {nick uhost hand chan arg} {
	global botversion
	set legit_cmd 1

	# forbidden task commands
	set forbidden_args { help edit command stats config ghistory burndown}
	foreach element $forbidden_args {
		if { $element == [lindex $arg 0] } {
			set legit_cmd 0
		}
	}

	# building command
	if { $legit_cmd } {
		set command task
		#lappend command "rc:~/.taskrc"
		lappend rc.confirmation:no
		lappend rc.verbose:list
		lappend rc.bulk:0
		lappend rc.recurrence.confirmation:yes
		foreach rec $arg {
			lappend command $rec
		}

		# decide by command type
		switch [lindex $arg 0] {
			add {
				# adding handle
				lappend command by
				lappend command $hand
				lappend command +top
			}
			"" {
				# list only
				lappend command +top
			}
			default {
				# nop
			}
		}

		set retval [ catch { exec {*}$command } msg ]
		set records [split $msg "\n"]
		foreach rec $records {
			putmsg $chan "$rec"
		}
	} else {
		putmsg $chan "command blocked"
	}
}


#------------------------------------------------------------------------------
# Setze topic mit nächstem #devlug Termin
#------------------------------------------------------------------------------
proc pub_topic {nick uhost hand chan arg} {
	global botversion
	global botnick

	set command stammtisch
	lappend command --irc
	set retval [ catch { exec {*}$command } msg ]
	# quick and dirty until stammtisch is updated
	set msg [ string range $msg 0 36 ]
	set prefix "Virtuelle Linux User Group - https://www.devlug.de -"
	set postfix "um 20:30 Uhr"
	putserv "TOPIC $chan :$prefix $msg $postfix"
}

#------------------------------------------------------------------------------
# Der Befehl !tux ist für die Hilfe
#------------------------------------------------------------------------------
proc pub_tux {nick uhost hand chan arg} {
        global botversion
        global botnick
	global help_file
        
        putmsg $chan "Hallo $nick! Ich bin $botnick $botversion. Alle weiteren Information gebe ich dir persönlich."


	# read mod-file
	set msg [ read_file $help_file ]

	set records [split $msg "\n"]
	foreach rec $records {
	    putmsg $nick "$rec"
	}

        putcmdlog "<$nick@$chan> !$hand! help $arg"
}

#------------------------------------------------------------------------------
# Wetter
# Syntax:
# !w <Stadt><,TDL>
#------------------------------------------------------------------------------
proc pub_weather {nick uhost hand chan arg} {
        global botversion
        global botnick
        global ansiweather

        set option1 "-afalse"
        set option2 "-l$arg"
        set option3 "-f3"

        if {$arg!=""} {
                set option2 "-l$arg"
        } else {
                set option2 ""
        }

        set retval [catch { exec $ansiweather $option1 $option2} msg ]
        putmsg $chan "$msg"
        if {$retval==0} {
                set retval [catch { exec $ansiweather $option1 $option3 $option2 } msg ]
                putmsg $chan "$msg"
        }
#        putcmdlog "<$nick@$chan> !$hand! help $arg"
}

#------------------------------------------------------------------------------
# little cve grabber
# Syntax:
# !cve <CVE-CVEID>
#------------------------------------------------------------------------------
proc longtextout {text} {
    set text [split [string map {\n " "} $text]]
    set curpos 0
    for {set x 1} {$x <= [llength $text]} {incr x} {
        if {[string length [join [lrange $text $curpos $x]]] > 400} {
            lappend curout [join [lrange $text $curpos [expr $x-1]]]
            set curpos $x
        }
    }
    if {$curpos != [expr [llength $text] - 1]} {lappend curout [join [lrange $text $curpos end]]}
    return $curout
}

proc pub_cve { nick uhost hand chan cveid } {
	set cveurl "http://cve.circl.lu/api/cve/"

	set command curl
	lappend command -s $cveurl$cveid
	lappend command | jq -j .summary,.cvss

	set retval [ catch { exec {*}$command } msg ]
	if { $retval == 0 && [lindex $msg 0] != {null} } {
		foreach curline [longtextout $msg] {putmsg $chan $curline}
#		putmsg $chan "$cveid: [lindex $msg 0] Score: [lindex $msg 1]"
	} else {
		putmsg $chan "$cveid CVEID not found ($retval)"
	}
}

#------------------------------------------------------------------------------
# Issue in git erstellen
# Syntax:
# issue eine tolle Idee <--label>
#------------------------------------------------------------------------------
proc pub_issue {nick uhost hand chan arg} {
	global git_token
	set giturl "https://gitlab.com/api/v4/projects/devLUG%2Fplanung-und-ideen/issues"
	set labeltypes { Bug Doing Idee "To Do" Diskussion Artikel Vorschlag Dokumentation Frage }

	# helper to format label
	proc upper1char {sentence} {
		subst -nobackslashes -novariables [regsub -all {\S+} $sentence {[string totitle &]}]
	}

    # get label
	set label [ lsearch -inline $arg --* ]
	set label [ string range $label 2 end ]
	set label [ upper1char $label ]

#	# check label
#	set legit 0
#	foreach element $labeltypes {
#		if { $element == $label } {
#			set legit 1
#		}
#	}

#	if { $legit == 0 } {
#		putmsg $chan "issue: Unbekanntes Label. \(Erlaubte Labels:$labeltypes\)"
#		return
#	}

	if { ( $arg == "" ) } {
		putmsg $chan "issue: usage: !issue <title> <--label>"
		return
    }

    # get title
    set title [ lsearch -inline -all -not $arg --* ]

    # build command
    set command curl
    lappend command -s --request POST
    lappend command --header "PRIVATE-TOKEN: $git_token"
    lappend command $giturl
    lappend command -d title=$title
    lappend command -d labels=$label

    # fire it up
    set retval [ catch { exec {*}$command } msg ]
    if { $retval == 0 } {
		putmsg $chan "issue: Issue created. Title: $title / Label: $label"
    } else {
		putmsg $chan "issue: failed ($retval)"
    }
}

#------------------------------------------------------------------------------
# read a file 
#------------------------------------------------------------------------------
proc read_file { my_file } {
    set fp [open "$my_file" r]
    set file_data [read $fp]
    close $fp
    return $file_data
}

#------------------------------------------------------------------------------
# Check einer Webseite
# Syntax:
# !webcheck <url>
#------------------------------------------------------------------------------

proc pub_webcheck {nick uhost hand chan arg} {
    global botversion
    global botnick

    set command curl
    lappend command -s
    lappend command -o /dev/null
    lappend command --user-agent
    lappend command Mozilla/4.0
    lappend command -L 
    lappend command $arg

    set TIME_start [clock clicks -milliseconds]
    set retval [ catch { exec {*}$command } msg ]
    set TIME_taken [expr [clock clicks -milliseconds] - $TIME_start]

    if { $retval == 0 } {
	putmsg $chan "webcheck: OK! $msg ($TIME_taken ms)"
    } else {
	putmsg $chan "webcheck: Site failed!"
    }
}

#------------------------------------------------------------------------------
# MOD
# Syntax:
# !mod
#------------------------------------------------------------------------------

proc pub_mod {nick uhost hand chan arg} {
    global botversion
    global botnick
    global mod_file

    # read mod-file
    set msg [ read_file $mod_file ]

    set records [split $msg "\n"]
    foreach rec $records {
	putmsg $chan "$rec"
    }
}

putlog "- $botname $botversion loaded"
