Ich überwache die Channels der devLUG. In allen offiziellen Channels der devLUG findest du mich.
Wenn du hilfe brauchst, dann gib im Channel !staff ein. Ich werde dann versuchen ein OP zu finden, der dir weiterhelfen kann.
Mit !seen <nick> kannst du Informationen über <nick> abrufen. Ich gebe aber nur Information über Mitglieder der devLUG.
Entscheidung: *d * - zeigt dir alle Entscheidungen an. ?d <Entscheidung> - gibt dir Infos zur Entscheidung
Hilfe: ** vLUG - Suche einen Eintrag. ?? <Eintrag> - gibt dir Informationen zum Eintrag.
!iam - Gibt dir Information über deinen User. Hilfreich für Mitglieder.
!toplist - Listet die TOPs für die nächste #devlug
!topadd <Beschreibung> - Fügt ein neuen TOP hinzu
