#!python3
from irc.client import SimpleIRCClient
from irc.connection import Factory
from configparser import ConfigParser
import csv
import json
import ssl
from http import client
from time import sleep

def nope(connection, user, channel, *args):
    connection.privmsg(channel, "Unbekanntes Kommando")

class Kommandos:
    def __init__(self, connection):
        self.c = connection
    def write(self, ziel: str, text: str):
        data = []
        if len(text) >= 100:
            for c in range(0, len(text)//100):
                data.append(text[c*100, (c+1)*100])
        else:
            data.append(text)
        self._write_multi(ziel, data)

    def _write_multi(self, ziel: str, data: list):
        for text in data:
            self.c.privmsg(ziel, text)
            sleep(0.4)

    def hallo(self, user, channel, ziel, *args):
        self.write(channel, "Hallo {} von {}".format(ziel, user))

    def hilfe(self, user, channel, *args):
        self._write_multi(channel, [
            "Hier sind meine Kommandos:",
            "!bot hallo <username>",
            "!bot hilfe",
            "!bot fluch <user>",
            "!bot wetter <ort>|<plz>",
        ])

    def fluch(self, user, channel, ziel, *args):
        if ziel == "stop5":
            text = "{} wünscht alles Gute an {}"
        else:
            text = "{} verflucht {} und dessen Kinder und Kindeskinder"
        self.write(channel, text.format(user, ziel))

    def wetter(self, user, channel, *ort):
        ort = " ".join(ort)
        try:
            plz = int(ort)
            if plz < 0:
                self._write_multi(channel, [
                    "10 Tagesvorraussage:",
                    "Tag 1: Sonne 30 °C",
                    "Tag 2: Wolkig 20 °C",
                    "Tag 3: Sturm 15 °C",
                    "Tag 4: Orkan 10 °C",
                    "Tag 5: Heuschrecken 15°C",
                    "Tag 6: Irrelevant",
                    "Tag 7: Anticrist",
                    "Tag 8: Unbekannt",
                    "Tag 9: Unbekannt",
                    "Tag 10: Unbekannt",
                    "Quelle: https://www.xkcd.com/1245/",
                ])
                return
            elif plz == 0:
                ort = "34346"
                self.write(channel, "Wieso willst du wissen wie das Wetter im Bierweg 1, Hann. Münden ist?!")
        except ValueError:
            pass
        call = "/data/2.5/weather?q={city_name},de&appid=85a4e3c55b73909f42c6a23ec35b7147&lang=de&units=metric".format(city_name=ort)
        c = client.HTTPSConnection("api.openweathermap.org")
        c.request("GET", call)
        resp = c.getresponse()
        data = json.load(resp)
        self.write(channel, "{} {}°C".format(data["weather"][0]["description"], data["main"]["temp"]))

class PyIrcBot(SimpleIRCClient):
    def __init__(self, server, username, password, realname, command_name, channels, kommandoklasse=Kommandos, port=6667):
        super().__init__()
        factory = Factory(wrapper=ssl.wrap_socket)
        self.connect(
            server=server,
            port=port,
            nickname=username,
            password=password,
            username=username,
            ircname=realname,
        )
        for channel in channels:
            if type(channel) == str:
                self.connection.join(channel)
            else:
                self.connection.join(*channel)
        self.command = command_name
        self.kommandos = kommandoklasse(self.connection)

    def on_pubmsg(self, connection, event):
        user = event.source.nick
        kanal = event.target
        text = event.arguments[0].split(" ")
        if self.command != text[0]:
            return
        if len(text) < 2:
            nope(connection, user, kanal)
            return
        c = text[1]
        if len(text) == 2:
            arg = []
        else:
            arg = text[2:]
        func = getattr(self.kommandos, c, None)
        if func:
            return func(user, channel, *arg)
        nope(connection, user, kanal)

if __name__ == "__main__":
    config = ConfigParser()
    config.read("config.ini")
    config = config["bot"]
    if not all((config["server"], config["username"], config["passwort"], config["realname"], config["kommando"], config["channels"])):
        print("Konfigurationsdatei nicht richtig befüllt")
    else:
        bot = PyIrcBot(server=config["server"], username=config["username"], password=config["passwort"], realname=config["realname"], command_name=config["kommando"], channels=config["channels"].split(","))
        bot.start()
